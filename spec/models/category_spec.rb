# == Schema Information
#
# Table name: categories
#
#  id                     :integer          not null, primary key
#  name                   :string(140)      not null
#  sell_in                :integer          default("0")
#  start_quality          :integer          default("0")
#  max_quality            :integer          default("0")
#  quality_direction      :integer          default("0")
#  quality_rule           :integer          default("0")
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  extended_quality_rules :text
#  finaly_drop_quality    :boolean          default("false")
#

require 'rails_helper'

describe Category, type: :model do
  context 'ActiveModel' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_most(140) }

    it { should validate_numericality_of(:sell_in).only_integer.is_greater_than_or_equal_to(0) }
    it { should validate_numericality_of(:start_quality).only_integer.is_greater_than_or_equal_to(0) }
    it { should validate_numericality_of(:max_quality).only_integer.is_greater_than_or_equal_to(0) }
    it { should validate_numericality_of(:quality_rule).only_integer.is_greater_than_or_equal_to(0) }
  end

  context 'ActiveRecord' do
    subject { Category.new(name: 'Some test name') }

    it { should have_db_column(:sell_in) }
    it { should have_db_column(:start_quality) }
    it { should have_db_column(:max_quality) }
    it { should have_db_column(:quality_direction) }
    it { should have_db_column(:quality_rule) }
    it { should have_db_column(:finaly_drop_quality) }

    it { should validate_uniqueness_of(:name) }
    it { should define_enum_for(:quality_direction) }
    it { should have_many(:products) }
  end

  context 'Methods' do
    let(:category) { create :category }

    context '#quality_direction_as_integer' do
      context 'with "up" value' do
        let(:category) { create(:category, quality_direction: "up") }

        it { expect(category.quality_direction_as_integer).to eq(1) }
      end

      context 'with "down" value' do
        let(:category) { create :category }

        it { expect(category.quality_direction_as_integer).to eq(-1) }
      end
    end

    context '#add_extended_quality_rule' do
      context 'should set values as hash pair' do
        before { category.add_extended_quality_rule(123,456) }
        it { expect(category.extended_quality_rules).to eq({"123" => 456}) }
      end

      context 'should set extended_quality_rules key as integer' do
        before { category.add_extended_quality_rule("asd123",456) }
        it { expect(category.extended_quality_rules).to eq({"0" => 456}) }
      end
    end

    context '#form_extended_quality_rules=' do
      context 'with empty hash' do
        before { category.form_extended_quality_rules = {} }
        it { expect(category.extended_quality_rules).to eq({}) }
      end

      context 'with correct hash data' do
        let(:hash) do 
          { "extended_days_left_1" => 10, "extended_rule_1" => 3, 
            "extended_days_left_2" => 5, "extended_rule_2" => 5 }
        end 
        before { category.form_extended_quality_rules = hash }
        it { expect(category.extended_quality_rules).to eq({ "10" => 3, "5" => 5 }) }
      end
    end
  end
end
