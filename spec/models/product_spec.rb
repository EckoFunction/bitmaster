# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  name        :string(140)      not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer
#

require 'rails_helper'

describe Product, type: :model do
  context 'ActiveModel' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_most(140) }
  end

  context 'ActiveRecord' do
    subject { Product.new(name: 'Some test name') }

    it { should have_db_column(:name) }
    it { should validate_uniqueness_of(:name) }
    it { should have_db_index(:name) }
    it { should belong_to(:category) }
  end

  context 'Methods' do
    let(:product) { create(:product) }

    context '#days_left' do
      context 'created today' do
        it { expect(product.days_left).to eq(3) }
      end

      context 'created 2 days ago' do
        before { product.update_attributes(created_at: 2.days.ago) }
        it { expect(product.days_left).to eq(1) }
      end
    end

    context '#days_passed' do
      context 'created today' do
        it { expect(product.days_passed).to eq(0) }
      end

      context 'created 2 days ago' do
        before { product.update_attributes(created_at: 2.days.ago) }
        it { expect(product.days_passed).to eq(2) }
      end
    end

    context '#quality_modification' do
      context 'without extended quality rules' do
        context 'return category.quality rule' do
          it { expect(product.quality_modification(1)).to eq(product.category.quality_rule) }
        end

        context 'return category.quality rule * 2' do
          before { product.update_attributes(created_at: product.category.sell_in.days.ago) }
          it { expect(product.quality_modification(4)).to eq(product.category.quality_rule * 2) }
        end      
      end

      context 'with extended quality rules' do
        before { product.category.add_extended_quality_rule(2,10) }

        context 'return category.quality rule' do
          it { expect(product.quality_modification(0)).to eq(1) }
        end

        context 'return extended quality rule' do 
          it { expect(product.quality_modification(1)).to eq(10) }
        end
      end
    end

    context '#quality' do
      let(:product) { create :product }

      context 'with start quality eq 50, rule eq 1 and down quality direction' do

        context 'with so much days left' do
          before { product.update_attributes(created_at: 100.days.ago) }
          it { expect(product.quality).to eq(0) }
        end

        context 'with 1 day passed' do
          before { product.update_attributes(created_at: 1.day.ago) }
          it { expect(product.quality).to eq(49) }
        end
      end

      context 'with start quality eq 50, rule eq 4 and down quality direction' do
        before { product.category.update_attributes(quality_rule: 4) }

        context 'with so much days left' do
          before { product.update_attributes(created_at: 100.days.ago) }
          it { expect(product.quality).to eq(0) }
        end

        context 'with 1 day passed' do
          before { product.update_attributes(created_at: 1.day.ago) }
          it { expect(product.quality).to eq(46) }
        end
      end

      context 'with start quality eq 25, rule eq 5 and up quality direction' do
        before { product.category.update_attributes(start_quality: 25, quality_rule: 5, quality_direction: :up) }

        context 'with so much days left' do
          before { product.update_attributes(created_at: 100.days.ago) }
          it { expect(product.quality).to eq(product.category.max_quality) }
        end

        context 'with 1 day passed' do
          before { product.update_attributes(created_at: 1.day.ago) }
          it { expect(product.quality).to eq(25 + 5*1) }
        end

        context 'with finaly_drop_quality' do
          before do 
            product.category.update_attributes(finaly_drop_quality: true)
            product.update_attributes(created_at: 100.days.ago)
          end
          it { expect(product.quality).to eq(0) }
        end
      end

    end
  end
end
