FactoryGirl.define do
  factory :category do
    sequence(:name) { |n| "Some test category name #{n}"}
    sell_in 3
    start_quality 50
    max_quality 50
    quality_rule 1
    finaly_drop_quality false

    transient do
      products_count 5
    end

    after(:create) do |category, evaluator|
      create_list(:product, evaluator.products_count, category: category)
    end
  end
end
