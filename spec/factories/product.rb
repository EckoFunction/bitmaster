FactoryGirl.define do
  factory :product do
    sequence(:name) { |n| "Some test product name #{n}"}
    category
  end
end