FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "some_#{n}@test.email" } 
    password '123456789'
    password_confirmation '123456789'
  end
end