require 'rails_helper'

describe Admin::CategoriesController, type: :controller do
  let(:resource_class) { "Category" }
  let(:resource) { ActiveAdmin.application.namespaces[:admin].resources[resource_class] }

  context 'for ActiveAdmin' do
    it { expect(resource.resource_name).to eq(resource_class) }
    it { expect(resource.defined_actions).to eq([:update, :index, :new, :create, :edit, :destroy, :show]) }
  end
end
