class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :name, unique: true, index: true, limit: 140, null:false
      t.integer :sell_in, default: 0
      t.integer :start_quality, default: 0
      t.integer :max_quality, default: 0
      t.integer :quality_direction, default: 0
      t.integer :quality_rule, default: 0

      t.timestamps
    end
  end
end
