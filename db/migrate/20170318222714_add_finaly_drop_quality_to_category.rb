class AddFinalyDropQualityToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :finaly_drop_quality, :boolean, default: false
  end
end
