class AddExtendedQualityRulesToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :extended_quality_rules, :text
  end
end
