class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name, unique: true, index: true, limit: 140, null:false

      t.timestamps
    end
  end
end
