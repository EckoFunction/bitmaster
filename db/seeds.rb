User.create!(
  email: 'admin@example.com', 
  password: 'password', 
  password_confirmation: 'password') unless User.find_by(email: 'admin@example.com')

categories = [
  {
    name: 'Aged Brie', 
    sell_in: 10, 
    start_quality: 0, 
    max_quality: 50, 
    quality_rule: 1, 
    quality_direction: :up
  },
  {
    name: 'Sulfuras', 
    sell_in: 10, 
    start_quality: 50, 
    max_quality: 50, 
    quality_rule: 0, 
    quality_direction: :up
  },
  {
    name: 'Backstage passes', 
    sell_in: 10, 
    start_quality: 0, 
    max_quality: 50, 
    quality_rule: 1, 
    quality_direction: :up,
    extended_quality_rules: { "10" => 2, "5" => 3 },
    finaly_drop_quality: true
  },
  {
    name: 'Conjured', 
    sell_in: 10, 
    start_quality: 50, 
    max_quality: 50, 
    quality_rule: 2, 
    quality_direction: :down
  }
]

categories.each do |data|
  c = Category.create(data) unless c = Category.find_by(name: data[:name])
  for i in 1..11 do
    product = c.products.find_or_create_by(name: "#{data[:name]} test product #{i}")
    product.update_attributes(created_at: i.days.ago)
  end
end
