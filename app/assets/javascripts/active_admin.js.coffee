#= require active_admin/base
#= require jquery

$ ->
  $('form.simple_form').on 'click', 'button.btn-add_rule', ->
    html = $(this).data('html')
    html = html.replace(/%INDEX%/g, $('.extended_rules .extended_rule_part').length / 2)
    $('.extended_rules').append(html)
