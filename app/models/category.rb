# == Schema Information
#
# Table name: categories
#
#  id                     :integer          not null, primary key
#  name                   :string(140)      not null
#  sell_in                :integer          default("0")
#  start_quality          :integer          default("0")
#  max_quality            :integer          default("0")
#  quality_direction      :integer          default("0")
#  quality_rule           :integer          default("0")
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  extended_quality_rules :text
#  finaly_drop_quality    :boolean          default("false")
#

class Category < ApplicationRecord

  enum quality_direction: [ :down, :up ], _prefix: true

  validates :name, presence: true, length: { maximum: 140 }, uniqueness: true

  validates :sell_in, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :start_quality, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :max_quality, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :quality_rule, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :finaly_drop_quality, inclusion: { in: [ true, false ] }

  serialize :extended_quality_rules, Hash

  has_many :products

  def quality_direction_as_integer
    quality_direction_down? ? -1 : 1
  end

  def add_extended_quality_rule(days_left, rule)
    extended_quality_rules[days_left.to_i.to_s] = rule.to_i
  end

  def form_extended_quality_rules=(hash)
    return if hash.blank?
    
    hash.stringify_keys!
    self.extended_quality_rules = {}
    for i in 0..hash.count do
      next if hash["extended_days_left_#{i}"].blank? || hash["extended_rule_#{i}"].blank?
      add_extended_quality_rule(hash["extended_days_left_#{i}"], hash["extended_rule_#{i}"])
    end
  end

end
 
