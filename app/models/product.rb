# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  name        :string(140)      not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer
#

class Product < ApplicationRecord
  validates :name, presence: true, length: { maximum: 140 }, uniqueness: true

  belongs_to :category

  def days_left
    get_positive_number(category.sell_in - ((Time.zone.now - created_at) / 1.day).to_i)
  end

  def days_passed
    get_positive_number(((Time.zone.now - created_at) / 1.day).to_i)
  end

  def quality_modification(modification_for_days_passed)
    modification_for_days_left = category.sell_in - modification_for_days_passed
    if category.extended_quality_rules.present?
      days_key = category.extended_quality_rules.
                  keys.
                  select{|rule_days_left| modification_for_days_left <= rule_days_left.to_i }.
                  map(&:to_i).min.to_s
      category.extended_quality_rules[days_key] || category.quality_rule
    else
      modification_for_days_left <= 0 ? category.quality_rule * 2 : category.quality_rule
    end 
  end

  def quality
    return 0 if category.finaly_drop_quality && days_left <= 0

    quality = category.start_quality
    days_passed.times do |i|
      break if quality > category.max_quality
      quality += quality_modification(i+1) * category.quality_direction_as_integer
    end
    quality = get_positive_number(quality)

    quality > category.max_quality ? category.max_quality : quality
  end

  protected

    def get_positive_number(number)
      number < 0 ? 0 : number
    end
end
