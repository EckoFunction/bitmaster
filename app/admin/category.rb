ActiveAdmin.register Category do
  
  permit_params :name, :sell_in, :start_quality, :max_quality, 
                :quality_rule, extended_quality_rules: [ :days_left , :rule ]


  index do
    id_column
    column :name
    column :sell_in
    column :start_quality
    column :max_quality
    column :quality_direction
    column :quality_rule
    column :finaly_drop_quality
    column :extended_quality_rules do |category| 
      category.extended_quality_rules.to_a.map{|pair| pair.join(', ')}.join('<br/>').html_safe
    end
    actions
  end

  form partial: 'form'

  member_action :update, method: [:patch, :put] do 
    if resource.update_attributes(category_params)
      redirect_to resource_path, notice: 'Category was saved successfuly'
    else
      render action: :edit
    end
  end

  controller do
    def category_params
      extended_rules = params[:category].delete(:form_extended_quality_rules)
      params.require(:category).permit(
        :name, 
        :sell_in, 
        :start_quality, 
        :max_quality, 
        :quality_rule).tap do |whitelisted| 
          whitelisted[:form_extended_quality_rules] = extended_rules.try(:permit!)
        end
    end
  end

end
