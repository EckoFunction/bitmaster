ActiveAdmin.register Product do
  
  permit_params :name, :category_id

  index do
    id_column
    column :name
    column :category
    column :created_at
    column :quality
    actions

  end
end
